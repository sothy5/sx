#ifndef SX_SET_IE_H
#define SX_SET_IE_H

#include "sx_msg.h"



/**
 * Creates & populates 'recovery/restart counter' information element
 *
 * @param header
 *   header pre-populated that contains transmission buffer for message
 * @return
 *   size of information element created in message
 */
uint16_t
set_recovery_ie(pfcp_header *header);

#endif /* SX_SET_IE_H */

#ifndef SX_FORWARDING_PARAMETERS
#define SX_FORWARDING_PARAMETERS

#include "sx_ie.h"
#include <stdio.h>


int create_sx_forwarding_parameters_ie(sx_ie *create_far_ie ){
      
       /* Destination Inferface, Network Instance, Redirect Information, Outer Header Creation, Transport level marking
       *  Forwarding Policy, Header Enrichment
       *
       *  Sxa :: Destination Inferface, Network Instance, Outer Header Creation, Transport level marking  
       */      
        
       sx_ie *forwarding_parameters_ie=(sx_ie *) ((uint8_t *) &create_far_ie->length+2+ ntohs(create_far_ie->length));
       forwarding_parameters_ie->type=htons(IE_FORWARDING_PARAMETERS);

       sx_ie *destination_interface= (sx_ie *) ((uint8_t *) &forwarding_parameters_ie->length+2);
       
       destination_interface->type=htons(IE_DESTINATION_INTERFACE);
       destination_interface->length=htons(1);
       *((uint8_t *)(&destination_interface->length)+2)=1;   /* Core network */
       
       forwarding_parameters_ie->length=htons(4+ntohs(destination_interface->length));
       printf("forwarding_parameters_ie %d\n", ntohs(forwarding_parameters_ie->length));       

       sx_ie *network_instance=(sx_ie *) ((uint8_t *) &destination_interface->length+2+ ntohs(destination_interface->length));
       network_instance->type=htons(IE_NETWORK_INSTANCE);
       char const * apn_name="apn.mobile";
       uint16_t len= sizeof(apn_name)/sizeof(char);
       for(int i=0; i< len;i++)
         *((uint8_t *)(&destination_interface->length)+2+i)=(uint8_t)apn_name[i];
       
      network_instance->length=htons(len);      
      forwarding_parameters_ie->length=htons(ntohs(forwarding_parameters_ie->length)+4+ntohs(network_instance->length));
      printf("forwarding_parameters_ie %d\n", ntohs(forwarding_parameters_ie->length));       

      
      sx_ie * outer_header_creation= (sx_ie *) ((uint8_t *) &network_instance->length+2+ ntohs(network_instance->length));
      outer_header_creation->type=htons(IE_OUTER_HEADER_CREATION);
      *((uint8_t *)(&outer_header_creation->length)+2)=GTP_U_UDP_IPV4;   /* Core network */
      
      uint32_t teid=1500;
                                         
      *((uint8_t *)(&outer_header_creation->length)+3)=(uint8_t)((teid>>24) & 0xFF);
      *((uint8_t *)(&outer_header_creation->length)+4)=(uint8_t)((teid>>16) & 0xFF);
      *((uint8_t *)(&outer_header_creation->length)+5)=(uint8_t)((teid>>8) & 0xFF);
      *((uint8_t *)(&outer_header_creation->length)+6)=(uint8_t)((teid) & 0xFF); 

     
      uint8_t destination_ipv4[] ={192,169,1,35};
      *((uint8_t *)(&outer_header_creation->length)+7)= destination_ipv4[0] ;
      *((uint8_t *)(&outer_header_creation->length)+8)= destination_ipv4[1];
      *((uint8_t *)(&outer_header_creation->length)+9)= destination_ipv4[2];
      *((uint8_t *)(&outer_header_creation->length)+10)= destination_ipv4[3]; 
      

      uint16_t port_no=35769;
      *((uint8_t *)(&outer_header_creation->length)+11)=(uint8_t)((port_no>>8) & 0xFF);
      *((uint8_t *)(&outer_header_creation->length)+12)=(uint8_t)((port_no) & 0xFF);
      
     outer_header_creation->length=htons(11);
      
     forwarding_parameters_ie->length=htons(ntohs(forwarding_parameters_ie->length)+4+ntohs(outer_header_creation->length));
     printf("forwarding_parameters_ie %d\n", ntohs(forwarding_parameters_ie->length));       
 
     

     sx_ie *transport_level_marking= (sx_ie *) ((uint8_t *) &outer_header_creation->length+2+ ntohs(outer_header_creation->length));
     transport_level_marking->type=htons(IE_TRANSPORT_LEVEL_MARKING);
     transport_level_marking->length=htons(2);
     *((uint8_t *)(&outer_header_creation->length)+2)==0; /*  IP Prec=0, IOS value=0  Best effort */
     *((uint8_t *)(&outer_header_creation->length)+3)==0;
     forwarding_parameters_ie->length=htons(ntohs(forwarding_parameters_ie->length)+4+ntohs(transport_level_marking->length));
     printf("forwarding_parameters_ie %d\n", ntohs(forwarding_parameters_ie->length));       

    
     create_far_ie->length=htons(ntohs(create_far_ie->length)+4+ntohs(forwarding_parameters_ie->length));
      

    return 0; 
      

       
     
}



#endif /* SX_FORWARDING_PARAMETERS */


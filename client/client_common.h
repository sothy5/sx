#ifndef SX_CP_CLIENT_COMMON

#define SX_CP_CLIENT_COMMON

static uint32_t sequencenumber=1000;
static uint8_t  cp_fun_features=0;

struct up_ip_resource_information{
       uint8_t ip_version; /*v4=1, v6=2 */
       uint8_t TEIDRI;
       uint8_t ASSONI;
       uint8_t teid_range;
       union address{
             uint8_t ip_v4[4];
             uint8_t ip_v6[16];
       }has_address ;
       uint8_t *network_instance;  

}; 
  

struct dp_node_id{
        uint8_t version; /* ipv4=1, ipv6=2 */
        unsigned long s_addr; 
        uint8_t fifth_bit_up_fun_features;
        uint8_t sixth_bit_up_fun_features;
        uint8_t is_associated; /* 1= when client receives sx_association_setup_response with success */
        struct up_ip_resource_information up_res_infor;        
       
};

struct session_context{
       uint64_t local_seid;
       uint64_t remote_seid;
       uint32_t sequence_number;
};

static struct session_context session_context_array[5];
static uint8_t sgw_c_assign_teid; /* 0= sgw_c_assign_teid, 1=sgw_U_assign_teid */ 
//static IP_resources;
 

static struct dp_node_id *dp_node_list;  /* work on single data path id */

static uint64_t seid=1000;
static uint8_t ipv4_sgw_pfcp[4]= {192, 168, 1,32};

static void create_seid(){
    
} 

static uint8_t T1;
static uint8_t N1;
static uint8_t PFCP_RETRANSMISSION_TIMER;


#endif /* SX_CP_CLIENT_COMMON */

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <stdio.h>
#include "../sx_association.h"
#include "../sx_msg.h"
#include "client_common.h"
#include "../sx_session.h"


int main(){
  int clientSocket, portNum, nBytes;
  char buffer[1024];
  struct sockaddr_in serverAddr;
  socklen_t addr_size;

  /*Create UDP socket*/
  clientSocket = socket(PF_INET, SOCK_DGRAM, 0);

  /*Configure settings in address struct*/
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(SX_UDP_PORT);
  serverAddr.sin_addr.s_addr = inet_addr("192.168.1.32");
  memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);  

  /*Initialize size variable to be used later on*/
  addr_size = sizeof serverAddr;

  uint8_t rx_buf[MAX_PFCP_UDP_LEN] = { 0 };
  uint8_t tx_buf[MAX_PFCP_UDP_LEN] = { 0 };

  pfcp_header *pfcp_tx = (pfcp_header *) tx_buf;
  
  create_sx_association_setup_request(pfcp_tx);
   
  printf("data in first byte: %d\n", tx_buf[0]);  
  printf("data in first byte: %d\n", tx_buf[1]);
  printf("data in first byte: %d\n", tx_buf[2]);
  printf("data in first byte: %d\n", tx_buf[3]);
  printf("data in first byte: %d\n", tx_buf[4]);
  printf("data in first byte: %d\n", tx_buf[5]);
  printf("data in first byte: %d\n", tx_buf[6]);
  printf("data in first byte: %d\n", tx_buf[7]);
   
  printf("Something new: %d\n", tx_buf[8]);
  printf("something new: %d\n", tx_buf[9]);
  
  printf("Something new: %d\n", tx_buf[10]);
  printf("something new: %d\n", tx_buf[11]);

  printf("Something new: %d\n", tx_buf[12]);
  printf("something new: %d\n", tx_buf[13]);
  
  printf("Node value: %d\n", tx_buf[14]);
  printf("Node value: %d\n", tx_buf[15]);
  printf("Node value: %d\n", tx_buf[16]);
  
  /*
   
  printf("recovery time: %d\n", tx_buf[17]);
  printf("recovery time: %d\n", tx_buf[18]);
  
  printf("recovery time: %d\n", tx_buf[19]);
  printf("recovery time: %d\n", tx_buf[20]);
  printf("recovery time: %d\n", tx_buf[21]);
  */
     
 
  
  printf("Version: %d\n", pfcp_tx->pfcp.version);  
  printf("data in first byte: %d\n", pfcp_tx->pfcp.mp);
  printf("data in first byte: %d\n", pfcp_tx->pfcp.s);
  printf("data in first byte: %d\n", pfcp_tx->pfcp.mtype);
  printf("length of the message: %d\n", ntohs(pfcp_tx->pfcp.mlength));
  //printf("sequnece number: %d\n",pfcp_tx->seid_header.no_seid.seq_num); 
  
  

  while(1){
    //printf("Type a sentence to send to server:\n");
    //fgets(buffer,1024,stdin);
    //printf("You typed: %d",pfcp_tx->pfcp.mlength);

    nBytes = strlen(buffer) + 1;
    
    /*Send message to server*/
    
    sendto(clientSocket,tx_buf,ntohs(pfcp_tx->pfcp.mlength)+4,0,(struct sockaddr *)&serverAddr,addr_size);

    /*Receive message from server*/
    
    nBytes = recvfrom(clientSocket,rx_buf,1024,0,NULL, NULL);
    pfcp_header *pfcp_rx = (pfcp_header *) rx_buf;
    bzero(tx_buf,1024);
    
    process_sx_association_setup_response(pfcp_rx,pfcp_tx);
       
    printf("Received from server: %d\n",ntohs(pfcp_rx->pfcp.mlength));
    bzero(tx_buf,MAX_PFCP_UDP_LEN);
    create_sx_session_establishment_request(pfcp_tx);
    sendto(clientSocket,tx_buf,ntohs(pfcp_tx->pfcp.mlength)+4,0,(struct sockaddr *)&serverAddr,addr_size);
    printf("Send sx_session_establishment request %d\n",ntohs(pfcp_tx->pfcp.mlength) );
    
    break;
  }
  


  return 0;
}

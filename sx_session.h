#ifndef SX_SESSION_H
#define SX_SESSION_H

#include "sx_ie.h"
#include "sx_msg.h" 

int create_sx_session_establishment_request(pfcp_header *header);

int process_sx_session_establishment_request(pfcp_header *header, pfcp_header *header_tx);

int create_sx_session_establishment_response(pfcp_header *header);

int process_sx_session_establishment_response(pfcp_header *header, pfcp_header *header_tx);

int create_sx_session_modification_request(pfcp_header *header);

int process_sx_session_modification_request(pfcp_header *header, pfcp_header *header_tx);

int create_sx_session_deletion_request(pfcp_header *header);

int process_sx_session_deletion_request(pfcp_header *header, pfcp_header *header_tx);





#endif /* SX_SESSION_H */




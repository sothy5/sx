#include "sx_create_far.h"
#include <arpa/inet.h>
#include <stdio.h>

#include "sx_forwarding_parameters.h"


int create_sx_create_far(pfcp_header *header){
        
        /* Create FAR    */
        /* { FARID, Apply Action, Forwarding Parameters, Duplicating Parametners, BAR ID }  */
      printf("Header length before create_far %d\n", ntohs(header->pfcp.mlength));
      sx_ie *create_far_ie= (sx_ie *)( ((uint8_t *) &header->seid_header)+ntohs(header->pfcp.mlength));
     
      create_far_ie->type=htons(IE_CREATE_FAR) ; 
            
  
      sx_ie *far_id_ie=(sx_ie *) ((uint8_t *)&create_far_ie->length +2); 
      far_id_ie->type=htons(IE_FAR_ID);
      
      far_id_ie->length=htons(4);
      *((uint8_t *)(&far_id_ie->length)+2)=255; /* 1 Rule is predefined in the UP Function  */
      *((uint8_t *)(&far_id_ie->length)+3)=0;
      *((uint8_t *)(&far_id_ie->length)+4)=0;
      *((uint8_t *)(&far_id_ie->length)+5)=0;
      
      create_far_ie->length=htons(4 + ntohs(far_id_ie->length));
      printf("create_far_ie_LENGTH %d\n", ntohs(create_far_ie->length));   
     
      sx_ie *apply_action= (sx_ie *) ((uint8_t *)&create_far_ie->length +2+ntohs(create_far_ie->length)); 
      apply_action->type=htons(IE_APPLY_ACTION);
      apply_action->length=htons(1);
      *((uint8_t *)(&far_id_ie->length)+2)=2; /* DUPL, NOCP, BUFF, FORW, DROP  */

      create_far_ie->length=htons(ntohs(create_far_ie->length)+4 + ntohs(apply_action->length));
      printf("create_far_ie_LENGTH %d\n", ntohs(create_far_ie->length));  
      /* Forwarding Parameters */
      
      create_sx_forwarding_parameters_ie(create_far_ie); 
       
      header->pfcp.mlength=htons (ntohs(header->pfcp.mlength)+4+ntohs(create_far_ie->length));
      printf("Header length after create_far_ie %d\n", ntohs(header->pfcp.mlength));
      /* BAR ID ommitted */
      return 0;     

}

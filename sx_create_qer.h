#ifndef SX_CREATE_QER
#define SX_CREATE_QER
#include "sx_msg.h"
#include "sx_ie.h"

int create_sx_create_qer(pfcp_header *header);
#endif /* SX_CREATE_QER */

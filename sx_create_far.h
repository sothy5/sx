#ifndef SX_CREATE_FAR
#define SX_CREATE_FAR

#include "sx_msg.h"
#include "sx_ie.h"

int create_sx_create_far(pfcp_header *header);

#endif /* SX_CREATE_FAR */

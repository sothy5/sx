#include "sx_set_ie.h"
#include "sx_msg.h"
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <ctype.h>
#include "sx_association.h"

/**
 * Main function - 
 * @param argc
 *   number of arguments
 * @param argv
 *   array of c-string arguments
 * @return
 *   returns 0
 */


int heart_beat_msg()
{
        uint8_t rx_buf[MAX_PFCP_UDP_LEN] = { 0 };
	uint8_t tx_buf[MAX_PFCP_UDP_LEN] = { 0 };

        pfcp_header *header= (pfcp_header *) rx_buf;
	set_recovery_ie(header);
      
	return 0;
}





int main(){
  int udpSocket, nBytes;
  char buffer[1024];
  struct sockaddr_in serverAddr, clientAddr;
  struct sockaddr_storage serverStorage;
  socklen_t addr_size, client_addr_size;
  int i;

  /*Create UDP socket*/
  udpSocket = socket(PF_INET, SOCK_DGRAM, 0);

  /*Configure settings in address struct*/
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(SX_UDP_PORT);
  serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
  memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);  

  /*Bind socket with address struct*/
  bind(udpSocket, (struct sockaddr *) &serverAddr, sizeof(serverAddr));

  /*Initialize size variable to be used later on*/
  addr_size = sizeof serverStorage;

  /* heart_beat_msg */
  pfcp_header *header= (pfcp_header *) buffer;
  set_recovery_ie(header);
  sendto(udpSocket,buffer,header->pfcp.mlength, 0,(struct sockaddr *)&serverStorage,addr_size);
  
  /* PFCP */
  
  uint8_t tx_buf[MAX_PFCP_UDP_LEN] = { 0 };
  pfcp_header *pfcp_tx = (pfcp_header *) tx_buf;
    
  
  

  while(1){
    /* Try to receive any incoming UDP datagram. Address and port of 
      requesting client will be stored on serverStorage variable */
    nBytes = recvfrom(udpSocket,buffer,1024,0,(struct sockaddr *)&serverStorage, &addr_size);
    
     

    /*Convert message received to uppercase*/
    for(i=0;i<nBytes-1;i++)
      buffer[i] = toupper(buffer[i]);
   
    pfcp_header *header= (pfcp_header *) buffer;
     

     if (nBytes > 0) {
         pfcp_header *header= (pfcp_header *) buffer;
         switch (header->pfcp.mtype){
                case SX_ASSOCIATION_SETUP_REQUEST:
                     process_sx_association_setup_request(header,pfcp_tx); 
                     break;
                case SX_ASSOCIATION_RELEASE_REQUEST:
                     break;
                case SX_SESSION_ESTABLISHMENT_REQUEST:
                      break;
                default:
                     break;
         }
         

     }    

    /*Send uppercase message back to client, using serverStorage as the address*/
    sendto(udpSocket,buffer,nBytes,0,(struct sockaddr *)&serverStorage,addr_size);
  }

  return 0;
}

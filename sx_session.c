#include "sx_session.h"
#include "sx_ie.h"
#include <stdio.h>
#include <string.h>
#include "client/client_common.h"
#include <arpa/inet.h>
#include <time.h>
#include "sx_create_far.h"
#include "sx_create_pdr.h"
#include "sx_create_urr.h"
#include "sx_msg.h"
#include "./server/server_common.h"

int create_sx_session_establishment_request(pfcp_header *header){
       struct session_context s_context;
       printf("session establishment request recevies %d \n", header->pfcp.mlength);
       header->pfcp.version=PFCP_VERSION;
       header->pfcp.mp=0;
       header->pfcp.s=1;
       header->pfcp.mtype=SX_SESSION_ESTABLISHMENT_REQUEST;
       header->seid_header.has_seid.seid=0;
       header->seid_header.has_seid.seq_num=htonl(s_context.sequence_number)>>8;
       header->seid_header.has_seid.mp=0;
       header->seid_header.has_seid.spare=0;
       
       header->pfcp.mlength= header->pfcp.s ? htons(sizeof(header->seid_header.has_seid)) : htons(sizeof(header->seid_header.no_seid));
       printf("session establishment request recevies %d \n", ntohs(header->pfcp.mlength));
        
       /* Node ID, CP F-SEID, Create PDR, Create FAR, Create URR, Create QER, Create BAR, PDN Type,
          SGW-C FQ-CSID, MME FQ-CSID, PGW-C FQ-CSID, ePDG FQ-CSID, TWAN FQ CSID */  
       
       /* Node id    IE */
       sx_ie *node_id=(sx_ie *) ( ((uint8_t *) &header->seid_header) +ntohs(header->pfcp.mlength)); 
  
       node_id->type=htons(IE_NODE_ID);
       node_id->length=htons(5);
       
       *((uint8_t *)(&node_id->length)+2)=0;      /* IPv4 address for Node id of controller */
       *((uint8_t *)(&node_id->length)+3)=192;
       *((uint8_t *)(&node_id->length)+4)=168;
       *((uint8_t *)(&node_id->length)+5)=1;
       *((uint8_t *)(&node_id->length)+6)=32;

        header->pfcp.mlength=htons(ntohs(header->pfcp.mlength)+4+ntohs(node_id->length));
        
        printf("session establishment request recevies %d \n", ntohs(header->pfcp.mlength));      

       /* CP F-SEID     */
       
       sx_ie *cp_f_seid= (sx_ie *)( ((uint8_t *) &header->seid_header) +ntohs(header->pfcp.mlength));
       cp_f_seid->type=htons(IE_F_SEID);
       
       cp_f_seid->length= htons(1+2+8+4); 
       uint8_t fifth_octet=0x02;   /* V4 bit for 5th octer of F-SEID  */
       uint64_t timestamp= (uint64_t)time(NULL);   /* SEID calculatin based on timestamp */  
       
       /*
       *  Adding session context
       *
       */
       
       s_context.local_seid=timestamp; 
       session_context_array[0]=s_context; 
                 
       *((uint8_t *)(&cp_f_seid->length)+2)=fifth_octet;
       //*((uint8_t *)(&cp_f_seid->length)+3)=(uint8_t)( (seid >> 8) & 0xFF);
       //*((uint8_t *)(&cp_f_seid->length)+4)=(uint8_t)( (seid  & 0xFF));
       *((uint8_t *)(&cp_f_seid->length)+3)=(uint8_t)( (timestamp >> 56) & 0xFF);
       *((uint8_t *)(&cp_f_seid->length)+4)=(uint8_t)( (timestamp >> 48) & 0xFF);
       *((uint8_t *)(&cp_f_seid->length)+5)=(uint8_t)( (timestamp >> 40) & 0xFF);
       *((uint8_t *)(&cp_f_seid->length)+6)=(uint8_t)( (timestamp >> 32) & 0xFF);
       *((uint8_t *)(&cp_f_seid->length)+7)=(uint8_t)( (timestamp >> 24) & 0xFF);
       *((uint8_t *)(&cp_f_seid->length)+8)=(uint8_t)((timestamp >> 16) & 0xFF);
       *((uint8_t *)(&cp_f_seid->length)+9)=(uint8_t)((timestamp>> 8)  & 0xFF);
       *((uint8_t *)(&cp_f_seid->length)+10)=(uint8_t) (timestamp  & 0xFF);         
       *((uint8_t *)(&cp_f_seid->length)+11)=ipv4_sgw_pfcp[0]; 
       *((uint8_t *)(&cp_f_seid->length)+12)=ipv4_sgw_pfcp[1];
       *((uint8_t *)(&cp_f_seid->length)+13)=ipv4_sgw_pfcp[2];
       *((uint8_t *)(&cp_f_seid->length)+14)=ipv4_sgw_pfcp[3];
   
       header->pfcp.mlength=htons(ntohs(header->pfcp.mlength)+4+ntohs(cp_f_seid->length));
       printf("session establishment request recevies %d \n", ntohs(header->pfcp.mlength));
             
       if (create_sx_create_pdr(header)!=0){
          printf("error in processing Create RDR ");
       }
         

       if (create_sx_create_far(header)!=0){
           printf("error in processing Creating FAR");
       }
       if (create_sx_create_urr(header)!=0){
           printf("error in processing creating URR");
       }
       
 
      
    
    
    /* Create URR    */
         /*   { URR ID, Measurement Method, Reporting Triggers, Measurement Period, Volume Threshold,
                Volume Threshold, Volume Quota, Time Threshold, Time Quota, Quota Holding Time
                Dropped DL traffic Threshold, Monitoring Time, Subsequent Volume Threshold,
                Subsequence Time Threshold, Inactivity Detection Time, Linked URR ID,Measurement Information
                Time Quota Mechanism  
          }   */
    /* Create BAR    */
    /* PDN Type      */
    /* SGW-C FQ-CSID */
    /* MME FQ_CSID   */
    /* PGW_C FQ-CSID  */
    
    /*  Concatnate the all messages */


}

int process_sx_session_establishment_request(pfcp_header *header, pfcp_header *header_tx){
    printf("session establishment request recevies %d \n", ntohs(header->pfcp.mlength));
    /* Sx_Session_Establishment_Request */
    /* Make a decision who allocated F-TEID? client allocated from Local F-TEID within PDI. 
       retrieve id SEID?
       Save IDs?
       What I do with SEID and sequence number?
       SEID initally zero. 
     */
    
    if (header->pfcp.version==PFCP_VERSION && header->pfcp.s){
    
       if (htons(header->pfcp.mlength) > 13) {             
           printf("Processed sx_session_establishment_request \n" );
           
          
      
            sx_ie *first_ie=IE_BEGIN(header);
            struct cp_node_id_in_server node_id;
            
            if (ntohs(first_ie->type)==IE_NODE_ID ) {
                if(ntohs(first_ie->length)== 5 && *((uint8_t *)(&first_ie->length)+2)==0){
                   
                    uint8_t ip[]={*((uint8_t *)(&first_ie->length)+3) , *((uint8_t *)(&first_ie->length)+4) , *((uint8_t *)(&first_ie->length)+5) , *((uint8_t *)(&first_ie->length)+6)};
                  
                   //struct inet_addr node1=inet_addr(ip);
                   
                   node_id.version=0;
                   inet_pton(AF_INET, (char *)ip, &node_id.s_addr);
               }
            }
            /*  What to do with Node ID. detect coming from same node or different?*/
            
            struct session_context s_context=session_context_array[0];
              
            
            sx_ie *second_ie=NEXT_IE(first_ie);
            if (ntohs(second_ie->type)==IE_F_SEID && ntohs(second_ie->length)==13){
                uint8_t established_client_seid[8];  
                established_client_seid[0]= *((uint8_t *)&second_ie->length+3);
                established_client_seid[1]= *((uint8_t *)&second_ie->length+4);
                established_client_seid[2]= *((uint8_t *)&second_ie->length+5);
                established_client_seid[3]= *((uint8_t *)&second_ie->length+6);
                established_client_seid[4]= *((uint8_t *)&second_ie->length+7);
                established_client_seid[5]= *((uint8_t *)&second_ie->length+8);
                established_client_seid[6]= *((uint8_t *)&second_ie->length+9);
                established_client_seid[7]= *((uint8_t *)&second_ie->length+10);
                 
                s_context.remote_seid=0;
                s_context.remote_seid= (established_client_seid[0] << 56) & (established_client_seid[1]<< 48) & (established_client_seid[2]<<40) &(established_client_seid[3]<<32) &(established_client_seid[4]<<24) & (established_client_seid[5]<< 16) & (established_client_seid[6] << 8) & (established_client_seid[7]);
      
                /* IP address not checked,BUT SHOULD BE CHECKED */
                
            }
            
            sx_ie *create_pdr=NEXT_IE(second_ie);
            /* PDR ID, PDI, ASSUME that Local F-TEID sets to provide by UP function.*/
            
            sx_ie *pdr_id= NEXT_IE(create_pdr);
            printf("PRINTING PDR ID TYPE %d \n", ntohs(pdr_id->type));    
            
             
      }
     } 

    
    /* Sx_Session Establishment Response */
    /*Node ID, Cause, Offending IE, UP F-SEID,Created PDR, Load Control Information
      Overload control information, SGW-U FQ-CSID, PGW-U FQ-CSID, Failed Rule ID   
     */
     
     /*
     sx_ie *node_id_ie=(sx_ie *) ( ((uint8_t *) &pfcp_header_tx->seid_header) +ntohs(pfcp_header_tx->pfcp.mlength)); 
     node_id_ie->type=htons(IE_NODE_ID);
     node_id_ie->length=htons(5);
                     
     *((uint8_t *)(&node_id_ie->length)+2)=node_id_server_ip;
     *((uint8_t *)(&node_id_ie->length)+3)=node_id_server[0];
     *((uint8_t *)(&node_id_ie->length)+4)=node_id_server[1];
     *((uint8_t *)(&node_id_ie->length)+5)=node_id_server[2];
     *((uint8_t *)(&node_id_ie->length)+6)=node_id_server[3];

     sx_ie *cause=(sx_ie *) ( ((uint8_t *) &pfcp_header_tx->seid_header) +ntohs(pfcp_header_tx->pfcp.mlength)); 
     cause->type=htons(IE_CAUSE);
     cause->length=htons(1);
     *((uint8_t *)(&cause->length)+2)=1;  /* IRequest accepted (success)*/
     /*
     sx_ie *cp_f_seid= (sx_ie *)( ((uint8_t *) &header->seid_header) +ntohs(header->pfcp.mlength));
     cp_f_seid->type=htons(IE_F_SEID);
       
     cp_f_seid->length= htons(1+2+8+4); 
     uint8_t fifth_octet=0x02;   /* V4 bit for 5th octer of F-SEID  */
     /*
     uint64_t server_seid= (uint64_t)time(NULL);   
                 
     *((uint8_t *)(&cp_f_seid->length)+2)=fifth_octet;
     //*((uint8_t *)(&cp_f_seid->length)+3)=(uint8_t)( (seid >> 8) & 0xFF);
     //*((uint8_t *)(&cp_f_seid->length)+4)=(uint8_t)( (seid  & 0xFF));
     *((uint8_t *)(&cp_f_seid->length)+3)=(uint8_t)( (server_seid >> 56) & 0xFF);
     *((uint8_t *)(&cp_f_seid->length)+4)=(uint8_t)( (server_seid >> 48) & 0xFF);
     *((uint8_t *)(&cp_f_seid->length)+5)=(uint8_t)( (server_seid >> 40) & 0xFF);
     *((uint8_t *)(&cp_f_seid->length)+6)=(uint8_t)( (server_seid >> 32) & 0xFF);
     *((uint8_t *)(&cp_f_seid->length)+7)=(uint8_t)( (server_seid >> 24) & 0xFF);
     *((uint8_t *)(&cp_f_seid->length)+8)=(uint8_t)((server_seid >> 16) & 0xFF);
     *((uint8_t *)(&cp_f_seid->length)+9)=(uint8_t)((server_seid>> 8)  & 0xFF);
     *((uint8_t *)(&cp_f_seid->length)+10)=(uint8_t) (server_seid  & 0xFF);         
     *((uint8_t *)(&cp_f_seid->length)+11)=node_id_server[0]; 
     *((uint8_t *)(&cp_f_seid->length)+12)=node_id_server[1];
     *((uint8_t *)(&cp_f_seid->length)+13)=node_id_server[2];
     *((uint8_t *)(&cp_f_seid->length)+14)=node_id_server[3];
    
     */
      
     /* Created PDR IE */
     /* PDR ID, Local F-TEID */ /* Make a decision, Default SGW_C makes allocation F-TEID.*/ 
     
    
     

}


int create_sx_session_modification_request(pfcp_header *header){
       printf("session establishment request recevies %d \n", header->pfcp.mlength);
       header->pfcp.version=PFCP_VERSION;
       header->pfcp.mp=0;
       header->pfcp.s=1;
       header->pfcp.mtype=SX_SESSION_MODIFICATION_REQUEST;
       header->seid_header.has_seid.seid=session_context_array[0].remote_seid;
       header->seid_header.has_seid.seq_num=htonl(session_context_array[0].sequence_number+1)>>8;
       header->seid_header.has_seid.mp=0;
       header->seid_header.has_seid.spare=0;
       
       header->pfcp.mlength= header->pfcp.s ? htons(sizeof(header->seid_header.has_seid)) : htons(sizeof(header->seid_header.no_seid));
       /* CP F_SEID, Remove PDR, Remove FAR, Remove URR, Remove QER, Remove BAR, Create PDR,
        *  Create FAR, Create URR, Create QER, Create BAR, Update PDR, Update FAR, Update URR,        * 
        *  Update QER, Update BAR, SxSMReq_Flags, Query URR
        */
         
         /** Update PDR **/
         /* PDR ID, Outer Header Removal, PDI, FAR ID, URR ID, 
         *  QER ID, 
         *
         */
          
          /** Update FAR **/
          /**
          *   FAR ID, Apply Action, Update Forwarding Parameters, Update Duplicating Parameters, BAR ID
          *
          */
       sx_ie *node_id=(sx_ie *) ( ((uint8_t *) &header->seid_header) +ntohs(header->pfcp.mlength));
       


}

int process_sx_session_modification_request(pfcp_header *header, pfcp_header *header_tx){
     

}
int create_sx_session_modification_response(){
     /* Cause, Offending IE, Created PDR,Load Control Information, Overload control information
      * Usage report, Failed Rule ID
      */
}

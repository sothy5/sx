#ifndef __Sx_H__
#define __Sx_H__


/*
*
*   PFCP Message type definition 	and Sx header definition according to 3GPP
*   TS 29.244
*
*
*
*
*/


/*
*Sx Node related messages
*
*
*/

/*
#define SX_HEARTBEAT_REQUEST                 (1)
#define SX_HEARTBEAT_RESPONSE                (2)
#define SX_PFD_MANAGEMENT_REQUEST            (3)
#define SX_PFD_MANAGEMENT_RESPONSE           (4)
#define SX_ASSOCIATION_SETUP_REQUEST         (5)
#define SX_ASSOCIATION_SETUP_RESPONSE        (6)
#define SX_ASSOCIATION_UPDATE_REQUEST        (7)
#define SX_ASSOCIATION_UPDATE_RESPONSE       (8)
#define SX_ASSOCIATION_RELEASE_REQUEST       (9)
#define SX_ASSOCIATION_RELEASE_RESPONSE      (10) 
#define SX_VERSION_NOT_SUPPORTED_RESPONSE    (11)
#define SX_NODE_REPORT_REQUEST               (12)
#define SX_NODE_REPORT_RESPONSE              (13)
#define SX_SESSION_SET_DELETION_REQUEST      (14)
#define SX_SESSION_SET_DELETION_RESPONSE     (15)
*/
/*
*Sx session related messages
*
*
*/
/*
#define  SX_SESSION_ESTABLISHMENT_REQUEST     (50) 
#define  SX_SESSION_ESTABLISHMENT_RESPONSE    (51)
#define  SX_SESSION_MODIFICATION_REQUEST      (52)
#define  SX_SESSION_MODIFICATION_RESPONSE     (53)
#define  SX_SESSION_DELETION_REQUEST          (54)
#define  SX_SESSION_DELETION_RESPONSE         (55)
#define  SX_SESSION_REPORT_REQUEST            (56)
#define  SX_SESSION_REPORT_RESPONSE           (57) 


#pragma pack(1)

typedef struct pfcp_header_t{
        struct pfcp_t{
                uint8_t version:3;
                uint8_t spare:3;
                uint8_t mp:1;
                uint8_t s:1;
                uint8_t type;
                uint16_t length;

                
        } pfcp;
        union seid_u_t{
              struct has_seid_t{
                      uint64_t seid;
                      uint16_t seq:24;
                      uint8_t mp:4;
                      uint8_t spare:4;
              }has_seid;
              strucut no_seid_t{
                      uint32_t seq:24;
                      uint8_t spare;
              }no_seid;
        }seid_u;   
     

} pfcp_header;

*/

/**
*  Helper function to set the pfcp header for a PFCP message with seid field
*
*
*/

void set_pfcp_header(pfcp_header *pfcp_tc, uint8_t type, uint8_t has_teid, uint32_t seq); 


/**
*  
*
*
*
*/

int process_sx_association_setup_request(pfcp_header *pfcp_rx, pfcp_header *pfcp_tx);
 
int process_sx_association_setup_response(pfcp_header *pfcp_rx);

int process_sx_session_establishment_request(pfcp_request *pfcp_rx, pfcp_header *pfcp_tx);

int process_sx_session_establishment_response(pfcp_request *pfcp_rx, pfcp_header *pfcp_tx);

int create_sx_session_establishment_request(uint32_t sequence, pfcp_header *pfcp_tx);


#endif /* __Sx_H__ */

#ifndef SXMSG_H
#define SXMSG_H

#include <stdlib.h>
#include <stdint.h>

#define SX_UDP_PORT                            8805
#define PFCP_VERSION                           001
#define MAX_PFCP_UDP_LEN                       (1024)

/* Sx Node related messages */

#define SX_HEARTBEAT_REQUEST                     1
#define SX_HEARTBEAR_RESPONSE                    2
#define SX_PFD_MANAGEMENT_REQUEST                3
#define SX_PFD_MANAGEMENT_RESPONSE               4
#define SX_ASSOCIATION_SETUP_REQUEST             5
#define SX_ASSOCIATION_SETUP_RESPONSE            6
#define SX_ASSOCIATION_UPDATE_REQUEST            7
#define SX_ASSOCIATION_UPDATE_RESPONSE           8
#define SX_ASSOCIATION_RELEASE_REQUEST           9
#define SX_ASSOCIATION_RELEASE_RESPONSE         10 
#define SX_VERSION_NOT_SUPPORTED_RESPONSE       11
#define SX_NODE_REPORT_REQUEST                  12
#define SX_NODE_REPORT_RESPONSE                 13
#define SX_SESSION_SET_DELETION_REQUEST         14
#define SX_SESSION_SET_DELETION_RESPONSE        15

/* Sx session related messages */

#define  SX_SESSION_ESTABLISHMENT_REQUEST      50 
#define  SX_SESSION_ESTABLISHMENT_RESPONSE     51
#define  SX_SESSION_MODIFICATION_REQUEST       52
#define  SX_SESSION_MODIFICATION_RESPONSE      53
#define  SX_SESSION_DELETION_REQUEST           54
#define  SX_SESSION_DELETION_RESPONSE          55
#define  SX_SESSION_REPORT_REQUEST             56
#define  SX_SESSION_REPORT_RESPONSE            57 







/*

struct {
  uint32_t session_id;
  uint32_t rule_id;
  uint8_t precedence;
  
  struct {
     string src_interface;
     string ip_address;
     string network_instance;
     uint32_t local_fteid;
      
  } pdi;
   
   struct pdi _pdi;
}
   
 */ 
     
 



#pragma pack(1)


typedef struct pfcp_header_t{
      struct pfcp_t {
             uint8_t s :1;
             uint8_t mp :1;
             uint8_t spare :3;
             uint8_t version :3;
             uint8_t mtype;
             uint16_t mlength;
       } pfcp;
       union seid_t{
             struct pfcp_node_t{
                    uint64_t seid;
                    uint32_t  seq_num :24;
                    uint32_t mp :4;
                    uint32_t spare :4; 
             }has_seid;
            struct  pfcp_session_t{
                    uint32_t seq_num :24;
                    uint32_t spare :8;
                    
               
             }no_seid;
 
         }seid_header;   

} pfcp_header;

#pragma pack()



/**
 * Macro to provide address of first Information Element within message buffer
 * containing PFCP header. Address may be invalid and must be validated to ensure
 * it does not exceed message buffer.
 * @param pfcp_h
 *   Pointer of address of message buffer containing PFCP header.
 * @return
 *   Pointer of address of first Information Element.
 */
#define IE_BEGIN(pfcp_h)                               \
	  ((pfcp_h)->pfcp.s                              \
	  ? (sx_ie *)((&(pfcp_h)->seid_header.has_seid)+1)      \
	  : (sx_ie *)((&(pfcp_h)->seid_header.no_seid)+1))

#define NEXT_IE(pfcp_h) \
	(sx_ie *)((uint8_t *)(&pfcp_h->length) \
	+2+ntohs(pfcp_h->length))






#endif /* SXMSG_H */


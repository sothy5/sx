#ifndef SX_CREATE_URR
#define SX_CREATE_URR

#include "sx_msg.h"
#include "sx_ie.h"

int create_sx_create_urr(pfcp_header *header);

#endif /* SX_CREATE_URR */

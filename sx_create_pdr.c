#include "sx_create_pdr.h"
#include <arpa/inet.h>
#include <stdio.h>

int create_pdi_ie(sx_ie *create_pdr_ie){        
       
       /* PDI IE */
       /* Source Interface IE, Local F-TEID IE, Network instance, UE IP address, SDF Filter, Applciation ID */

        sx_ie *pdi_ie=(sx_ie *)( (uint8_t *)&create_pdr_ie->length +2+ntohs(create_pdr_ie->length));
        pdi_ie->type= htons(IE_PDI);
        
        
      
       
        
       /* Adding only source interface IE and local F-TEID IE to PDI IE */
       sx_ie *source_interface= (sx_ie *) ((uint8_t *)&pdi_ie->length+ 2);
       source_interface->type= htons(IE_SOURCE_INTERFACE);
       source_interface->length=htons(1);
       *((uint8_t *)(&source_interface->length)+2)=access;      
        
       pdi_ie->length=htons(4+ntohs(source_interface->length));
       printf("create_pdi_ie %d\n", ntohs(pdi_ie->length));       
 
       sx_ie *l_f_teid=(sx_ie *) ((uint8_t *) &pdi_ie->length +2+ntohs(pdi_ie->length));
        
         
        uint8_t fifth_octet_si=4;        /*0000 0100  CH=1 UP shall assign F-TEID */
        //uint8_t teid[]={0,0,0,100};
        //uint8_t ipv4[]={192,168, 1, 33};    /* how to detect the IP v4 address of UP */
        l_f_teid->length=htons(1);
        *((uint8_t *)(&l_f_teid->length)+2)=fifth_octet_si;
         
       pdi_ie->length= htons(ntohs(pdi_ie->length)+4+ntohs(l_f_teid->length));

       /* Network instance is omited */
       create_pdr_ie->length=htons(ntohs(create_pdr_ie->length)+4+ntohs(pdi_ie->length));
       printf("create_pdi_ie %d\n", ntohs(pdi_ie->length));       
        return 0;           
      
       
 
  
}


int create_sx_create_pdr(pfcp_header *header){
      
      

     /* Create PDR    */
       /* {PDR ID,Precedence, PDI, Outer Header remove, FAR ID, URR ID,QER ID,Activate Predefined Rules  } */       
       sx_ie *create_pdr_ie= (sx_ie *)( ((uint8_t *) &header->seid_header)+ntohs(header->pfcp.mlength));
       create_pdr_ie->type=htons(IE_CREATE_PDR);
       
       /* PDR ID */ 
       sx_ie *pdr_id=(sx_ie *) (((uint8_t *) &create_pdr_ie->length) +2);
       
       pdr_id->type=htons(IE_PACKET_DETECTION_RULE_ID);
       pdr_id->length=htons(2);
       *((uint8_t *)(&pdr_id->length)+2)=0x00;
       *((uint8_t *)(&pdr_id->length)+3)=0x12;
       
       create_pdr_ie->length=htons(4+ntohs(pdr_id->length));
       printf("create_pdr_ie %d\n", ntohs(create_pdr_ie->length));
       //header->pfcp.mlength=htons(ntohs(header->pfcp.mlength)+4+ntohs(create_pdr_ie->length));
         
       /* precedence */    
       /* Not applied for Sx_a interface */
       /*  
       sx_ie *precedence_ie=(sx_ie *) (((uint8_t *) &header->length) +2+4+ntohs(pdr_id->length)); ;
       precedence_ie->type=htons(IE_PRECEDENCE);
       precedence_ie->length=htons(4);
       uint8_t pre[]={0,0,0,100};
       */
       create_pdi_ie(create_pdr_ie);
        
       printf("create_pdr_ie After create_pdi_ie %d\n", ntohs(create_pdr_ie->length));
       //header->pfcp.mlength=htons(ntohs(header->pfcp.mlength)+4+ntohs(create_pdr_ie->length));
       
       sx_ie *outer_header_removal_ie=(sx_ie *) (((uint8_t *) &create_pdr_ie->length)+2+ntohs(create_pdr_ie->length));
       outer_header_removal_ie->type= htons(IE_OUTER_HEADER_REMOVAL);
       outer_header_removal_ie->length=htons(1);
       *((uint8_t *)(&outer_header_removal_ie->length)+2)=GTP_U_UDP_IPV4;
       create_pdr_ie->length=htons(ntohs(create_pdr_ie->length)+4+ntohs(outer_header_removal_ie->length));
       printf("create_pdr_ie %d\n", ntohs(create_pdr_ie->length));
       
       sx_ie *far_id_ie=(sx_ie *) (((uint8_t *) &create_pdr_ie->length)+2+ntohs(create_pdr_ie->length));
       far_id_ie->type=htons(IE_FAR_ID);
       far_id_ie->length=htons(4);
       *((uint8_t *)(&far_id_ie->length)+2)=255; /* 1 Rule is predefined in the UP Function  */
       *((uint8_t *)(&far_id_ie->length)+3)=0;
       *((uint8_t *)(&far_id_ie->length)+4)=0;
       *((uint8_t *)(&far_id_ie->length)+5)=0;
        
        
       create_pdr_ie->length=htons(ntohs(create_pdr_ie->length)+4+ntohs(far_id_ie->length));
       printf("create_pdr_ie %d\n", ntohs(create_pdr_ie->length));
       /* URR ID */              
        sx_ie *urr_id_ie=(sx_ie *) (((uint8_t *) &create_pdr_ie->length)+2+ntohs(create_pdr_ie->length));
        urr_id_ie->type=htons(IE_URR_ID);
        urr_id_ie->length=htons(4);
        
       *((uint8_t *)(&urr_id_ie->length)+2)=255; /* 1 Rule is predefined in the UP Function  */
       *((uint8_t *)(&urr_id_ie->length)+3)=0;
       *((uint8_t *)(&urr_id_ie->length)+4)=0;
       *((uint8_t *)(&urr_id_ie->length)+5)=0;
        create_pdr_ie->length=htons(ntohs(create_pdr_ie->length)+4+ntohs(urr_id_ie->length));
        printf("create_pdr_ie %d\n", ntohs(create_pdr_ie->length));
      
       /* Sxa interface not need QER ID and Activate Predefined rules */
       /*  QER ID */
       /*  
       sx_ie *qer_id_ie=(sx_ie *) (((uint8_t *) &create_pdr_ie->length)+2+ntohs(create_pdr_ie->length));
       qer_id_ie->type=htons(IE_URR_ID);
       qer_id_ie->length=htons(4);
        
       *((uint8_t *)(&qer_id_ie->length)+2)=255 /* 1 Rule is predefined in the UP Function  */
       /*
       *((uint8_t *)(&qer_id_ie->length)+3)=0;
       *((uint8_t *)(&qer_id_ie->length)+4)=0;
       *((uint8_t *)(&qer_id_ie->length)+5)=0;
        create_pdr_ie->length=htons(ntohs(ntohs(create_pdr_ie->length)+4+ntohs(urr_id_ie->length));


       /*Activate Predefiend rules */
       
       header->pfcp.mlength=htons(ntohs(header->pfcp.mlength)+4+ntohs(create_pdr_ie->length));
       printf("Header length after create_pdr_ie %d\n", ntohs(header->pfcp.mlength));
       return 0;
     
     
}  

        


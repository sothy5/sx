#ifndef SX_ASSOCIATION_H
#define SX_ASSOCIATION_H

#include "sx_msg.h"
#include "sx_ie.h"
#include <stdio.h>
#include <time.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include "./server/server_common.h"
#include "./client/client_common.h"





int create_sx_association_setup_request(pfcp_header *header){
       
       header->pfcp.version=PFCP_VERSION;
       header->pfcp.mp=0;
       header->pfcp.s=0;
       header->pfcp.mtype=SX_ASSOCIATION_SETUP_REQUEST;
       header->seid_header.no_seid.seq_num=htonl(sequencenumber)>>8;
       //htonl( 
       
       header->pfcp.mlength= header->pfcp.s ?htons(sizeof(header->seid_header.has_seid)) : htons(sizeof(header->seid_header.no_seid));        
       printf("message size: %d\n", ntohs(header->pfcp.mlength));
    
      /* Sx_association_setup_request */
      /* Node ID, Recovery Time Stamp, UP FUnction Features, CP Function Features, User Plane IP Resource Information */
      
       

       sx_ie *node_id=(sx_ie *) ( ((uint8_t *) &header->seid_header) +ntohs(header->pfcp.mlength)); 
  
      
       node_id->type=htons(IE_NODE_ID);
       node_id->length=htons(5);
       
       *((uint8_t *)(&node_id->length)+2)=0;      /* IPv4 address for Node id of controller */
       *((uint8_t *)(&node_id->length)+3)=192;
       *((uint8_t *)(&node_id->length)+4)=168;
       *((uint8_t *)(&node_id->length)+5)=1;
       *((uint8_t *)(&node_id->length)+6)=32;

         
       
       header->pfcp.mlength=htons(ntohs(header->pfcp.mlength)+4+ntohs(node_id->length));
       printf("message size: %d\n", ntohs(header->pfcp.mlength));
       
     
       sx_ie *recovery_time_stamp =(sx_ie *) ( ((uint8_t *) &header->seid_header) + ntohs(header->pfcp.mlength));
       
       recovery_time_stamp->type = htons(IE_RECOVERY_TIME_STAMP);
       recovery_time_stamp->length =htons(8) ;
       uint64_t timestamp= (uint64_t)time(NULL);
       
       uint8_t timestamp_value[8]={ (uint8_t)( (timestamp >> 56) & 0xFF),
                                    (uint8_t)( (timestamp >> 48) & 0xFF),
                                    (uint8_t)( (timestamp >> 40) & 0xFF),
                                    (uint8_t)( (timestamp >> 32) & 0xFF),
                                    (uint8_t)( (timestamp >> 24) & 0xFF),
                                    (uint8_t)( (timestamp >> 16) & 0xFF),
                                    (uint8_t)( (timestamp >> 8) & 0xFF),
                                    (uint8_t)( (timestamp & 0xFF)) };
       
       *((uint8_t *)(&recovery_time_stamp->length)+2)=timestamp_value[0];
       *((uint8_t *)(&recovery_time_stamp->length)+3)=timestamp_value[1];
       *((uint8_t *)(&recovery_time_stamp->length)+4)=timestamp_value[2];
       *((uint8_t *)(&recovery_time_stamp->length)+5)=timestamp_value[3];
       *((uint8_t *)(&recovery_time_stamp->length)+6)=timestamp_value[4];
       *((uint8_t *)(&recovery_time_stamp->length)+7)=timestamp_value[5];
       *((uint8_t *)(&recovery_time_stamp->length)+8)=timestamp_value[6];
       *((uint8_t *)(&recovery_time_stamp->length)+9)=timestamp_value[7];        
       

       header->pfcp.mlength=htons( ntohs(header->pfcp.mlength)+4+ ntohs(recovery_time_stamp->length));
       printf("message size: %d\n", ntohs(header->pfcp.mlength));
       
       /*        
       sx_ie *up_function_features;
       */
       
       sx_ie *cp_function_features=(sx_ie *) ( ((uint8_t *) &header->seid_header) + ntohs (header->pfcp.mlength));
       cp_function_features->type=htons(IE_CP_FUNCTION_FEATURES);
       cp_function_features->length=htons(1);
      *((uint8_t *)(&cp_function_features->length)+2)=0; /* LOAD and OVRL sets off */
       
       header->pfcp.mlength=htons(ntohs(header->pfcp.mlength)+4+ ntohs(cp_function_features->length));
       printf("message size: %d\n", ntohs(header->pfcp.mlength));
       
       

       /*
       sx_ie *up_ip_resource_information;
        */
        return 0; 
}
  
int create_sx_association_setup_response(pfcp_header *header) {
       sx_ie *node_id;
       sx_ie *cause;
       sx_ie *recovery_time_stamp;
       sx_ie *up_function_features;
       sx_ie *cp_function_features;
       sx_ie *up_ip_resource_informatin;
     
       return 0;
} 

int process_sx_association_setup_request(pfcp_header * pfcp_header_rx, pfcp_header *pfcp_header_tx ){
    
    printf("Processed sx_assocation_setup %d\n", ntohs(pfcp_header_rx->pfcp.mlength) );
    if (pfcp_header_rx->pfcp.version==PFCP_VERSION){
    
       if (htons(pfcp_header_rx->pfcp.mlength) > 13) {   /* pfcp header with Node id filed */
          /*Sx Association Setup Response with a successful cause  */
           printf("Processed sx_assocation_setup \n" );
      
            sx_ie *first_ie=IE_BEGIN(pfcp_header_rx);
            struct cp_node_id_in_server node_id;
            
            if (ntohs(first_ie->type)==IE_NODE_ID ) {
                if(ntohs(first_ie->length)== 5 && *((uint8_t *)(&first_ie->length)+2)==0){
                   
                    uint8_t ip[]={*((uint8_t *)(&first_ie->length)+3) , *((uint8_t *)(&first_ie->length)+4) , *((uint8_t *)(&first_ie->length)+5) , *((uint8_t *)(&first_ie->length)+6)};
                  
                   //struct inet_addr node1=inet_addr(ip);
                   
                   node_id.version=0;
                   inet_pton(AF_INET, (char *)ip, &node_id.s_addr);
                   
                   
                    
                   sx_ie *second_ie=NEXT_IE(first_ie);
                   sx_ie *cp_fun_feat=NEXT_IE(second_ie);
              
                   printf("cp_fun_feat_type %d\n",ntohs(cp_fun_feat->type));
                   printf("cp_fun_feat_length %d\n",ntohs(cp_fun_feat->length));                      
                   printf("cp_fun_feat_value %d\n", *((uint8_t *) (&cp_fun_feat->length)+2));
                   
                   node_id.cp_function_feature=*((uint8_t *) (&cp_fun_feat->length)+2);
                   node_list= &node_id;
                    
                   printf("Processed sx_assocation_setup...... \n" );
                    }
                 }
                 
                   
                   
                   pfcp_header_tx->pfcp.version=PFCP_VERSION;
                   pfcp_header_tx->pfcp.mp=0;
                   pfcp_header_tx->pfcp.s=0;
                   pfcp_header_tx->pfcp.mtype=SX_ASSOCIATION_SETUP_RESPONSE;
                   pfcp_header_tx->seid_header.no_seid.seq_num=pfcp_header_rx->seid_header.no_seid.seq_num;
                   pfcp_header_tx->pfcp.mlength= pfcp_header_tx->pfcp.s ?htons(sizeof(pfcp_header_tx->seid_header.has_seid)) : htons(sizeof(pfcp_header_tx->seid_header.no_seid));
                  
                  
                     
                   
                   /* Sx_Association_Setup_Response  */ 
                   /* Node Id, Cause, Recovery time, UP Function Features, CP Function Features, Userplane IP resoruce information   */


                   sx_ie *node_id_ie=(sx_ie *) ( ((uint8_t *) &pfcp_header_tx->seid_header) +ntohs(pfcp_header_tx->pfcp.mlength)); 
                   node_id_ie->type=htons(IE_NODE_ID);
                   node_id_ie->length=htons(5);
                     
                   *((uint8_t *)(&node_id_ie->length)+2)=0;
                   *((uint8_t *)(&node_id_ie->length)+3)=node_id_server[0];
                   *((uint8_t *)(&node_id_ie->length)+4)=node_id_server[1];
                   *((uint8_t *)(&node_id_ie->length)+5)=node_id_server[2];
                   *((uint8_t *)(&node_id_ie->length)+6)=node_id_server[3];

               
                   pfcp_header_tx->pfcp.mlength=htons(ntohs(pfcp_header_tx->pfcp.mlength)+4+ntohs(node_id_ie->length));
                   printf("message size: %d\n", ntohs(pfcp_header_tx->pfcp.mlength));
                     
                   sx_ie *cause=(sx_ie *) ( ((uint8_t *) &pfcp_header_tx->seid_header) +ntohs(pfcp_header_tx->pfcp.mlength)); 
                   cause->type=htons(IE_CAUSE);
                   cause->length=htons(1);
                   *((uint8_t *)(&cause->length)+2)=1;  /* IRequest accepted (success)*/
                   pfcp_header_tx->pfcp.mlength=htons(ntohs(pfcp_header_tx->pfcp.mlength)+4+ntohs(cause->length));
                   printf("message size: %d\n", ntohs(pfcp_header_tx->pfcp.mlength));
                   
                   sx_ie *recovery_time_stamp =(sx_ie *) ( ((uint8_t *) &pfcp_header_tx->seid_header) + ntohs(pfcp_header_tx->pfcp.mlength));
                   recovery_time_stamp->type = htons(IE_RECOVERY_TIME_STAMP);
                   recovery_time_stamp->length =htons(8) ;
                   uint64_t timestamp= (uint64_t)time(NULL);
       
                   uint8_t timestamp_value[8]={ (uint8_t)( (timestamp >> 56) & 0xFF),
                                    (uint8_t)( (timestamp >> 48) & 0xFF),
                                    (uint8_t)( (timestamp >> 40) & 0xFF),
                                    (uint8_t)( (timestamp >> 32) & 0xFF),
                                    (uint8_t)( (timestamp >> 24) & 0xFF),
                                    (uint8_t)( (timestamp >> 16) & 0xFF),
                                    (uint8_t)( (timestamp >> 8) & 0xFF),
                                    (uint8_t)( (timestamp & 0xFF)) };
       
                  *((uint8_t *)(&recovery_time_stamp->length)+2)=timestamp_value[0];
                  *((uint8_t *)(&recovery_time_stamp->length)+3)=timestamp_value[1];
                  *((uint8_t *)(&recovery_time_stamp->length)+4)=timestamp_value[2];
                  *((uint8_t *)(&recovery_time_stamp->length)+5)=timestamp_value[3];
                  *((uint8_t *)(&recovery_time_stamp->length)+6)=timestamp_value[4];
                  *((uint8_t *)(&recovery_time_stamp->length)+7)=timestamp_value[5];
                  *((uint8_t *)(&recovery_time_stamp->length)+8)=timestamp_value[6];
                  *((uint8_t *)(&recovery_time_stamp->length)+9)=timestamp_value[7]; 
                   
                   pfcp_header_tx->pfcp.mlength=htons(ntohs(pfcp_header_tx->pfcp.mlength)+4+ntohs(recovery_time_stamp->length));

                   sx_ie *up_function_features =(sx_ie *) ( ((uint8_t *) &pfcp_header_tx->seid_header) + ntohs(pfcp_header_tx->pfcp.mlength));
                   up_function_features->type = htons(IE_UP_FUNCTION_FEATURES);
                   up_function_features->length =htons(2);
                   *((uint8_t *)(&up_function_features->length)+2)=255; /*  */
                   *((uint8_t *)(&up_function_features->length)+3)=1; 
                   
                    printf("message size: %d\n", ntohs(pfcp_header_tx->pfcp.mlength));   
                    
                    /* User Plane IP Resource Information  */  
                    /* Network instance value */
                                      
                   
                                     
                                           

               
                          
            
           

                
       } 
   }else{
      /* Sx Version Not Supported Response */

       pfcp_header_tx->pfcp.version=PFCP_VERSION;
       pfcp_header_tx->pfcp.mp=0;
       pfcp_header_tx->pfcp.s=0;
       pfcp_header_tx->pfcp.mtype=SX_VERSION_NOT_SUPPORTED_RESPONSE;
       pfcp_header_tx->seid_header.no_seid.seq_num=pfcp_header_rx->seid_header.no_seid.seq_num;
       pfcp_header_tx->pfcp.mlength= pfcp_header_tx->pfcp.s ?htons(sizeof(pfcp_header_tx->seid_header.has_seid)) : htons(sizeof(pfcp_header_tx->seid_header.no_seid)); 
       
   }

}

int process_sx_association_setup_response(pfcp_header * pfcp_header_rx, pfcp_header *pfcp_header_tx){
    printf("process_sx_association_setup_response");
    if(pfcp_header_rx->pfcp.version==PFCP_VERSION && pfcp_header_rx->pfcp.mtype==SX_ASSOCIATION_SETUP_RESPONSE){
           
            sx_ie *first_ie=IE_BEGIN(pfcp_header_rx);
            struct dp_node_id node_id;
            
            if (ntohs(first_ie->type)==IE_NODE_ID ) {
                if(ntohs(first_ie->length)== 5 && *((uint8_t *)(&first_ie->length)+2)==0){
                   
                    uint8_t ip[]={*((uint8_t *)(&first_ie->length)+3) , *((uint8_t *)(&first_ie->length)+4) , *((uint8_t *)(&first_ie->length)+5) , *((uint8_t *)(&first_ie->length)+6)};
                  
                   //struct inet_addr node1=inet_addr(ip);
                   
                   node_id.version=0;
                   inet_pton(AF_INET, (char *)ip, &node_id.s_addr);
                   
                   
                    
                   sx_ie *second_ie=NEXT_IE(first_ie);
                   if (second_ie->type==IE_CAUSE){
                       node_id.is_associated= *((uint8_t *)(&second_ie->length)+2);
                   }
                                         

                   sx_ie *recovery_time=NEXT_IE(second_ie);
                   sx_ie *up_function_features=NEXT_IE(recovery_time);
                   
              
                 
                   
                   node_id.fifth_bit_up_fun_features=*((uint8_t *) (&up_function_features->length)+2);
                   node_id.sixth_bit_up_fun_features=*((uint8_t *) (&up_function_features->length)+3);
                   

                   /* */
                   struct up_ip_resource_information up_res_inf;
                   up_res_inf.ip_version=1;
                   up_res_inf.TEIDRI=0;
                   up_res_inf.ASSONI=1;
                   up_res_inf.has_address.ip_v4[0] =192;
                   up_res_inf.has_address.ip_v4[1]=168;
                   up_res_inf.has_address.ip_v4[2]=1;
                   up_res_inf.has_address.ip_v4[3]=32;
                   up_res_inf.network_instance="internet.mnc012.mcc345.gprs";
                   
                   node_id.up_res_infor=up_res_inf;
                   dp_node_list= &node_id;
                    
                   printf("Processed sx_assocation_setup...... \n" );
                }
            }
         
          
    }
    
}



#endif /* SX_ASSOCIATION_H */   

#include "sx_set_ie.h"
#include "sx_ie.h"
#include <time.h> 


#define IE_RECOVERY_TIME_STAMP                  (96)

/**
 * helper function to get the location of the next information element '*ie'
 * that the buffer located at '*header' may be used, in the case that the size
 * of the information element IS known ahead of time
 * @param header
 *   header pre-populated that contains transmission buffer for message
 * @param type
 *   information element type value as defined in 3gpp 29.274 table 8.1-1
 * @param instance
 *   Information element instance as specified by 3gpp 29.274 clause 6.1.3
 * @param length
 *   size of information element created in message
 * @return
 *   information element to be populated
 */

/*
static sx_ie_recovery *
set_next_ie(pfcp_header *header, uint16_t type)
{
	sx_ie_recovery_ie *ie = (sx_ie_recovery *) (((uint8_t *) &header->teid_u)
	    + ntohs(header->gtpc.length));

	if (ntohs(header->gtpc.length) + length
	    + sizeof(gtpv2c_ie) > MAX_GTPV2C_LENGTH) {
		rte_panic("Insufficient space in UDP buffer for IE\n");
	}

	header->gtpc.length = htons(
	    ntohs(header->gtpc.length) + length + sizeof(gtpv2c_ie));

	

	return ie;
}
*/



/**
 * helper function to set general value within an inforation element of size
 * 1 byte
 * @param header
 *   header pre-populated that contains transmission buffer for message
 * @param type
 *   information element type value as defined in 3gpp 29.274 table 8.1-1
 * @param instance
 *   Information element instance as specified by 3gpp 29.274 clause 6.1.3
 * @param value
 *   value of information element
 * @return
 */

/*
static uint16_t
set_uint32_ie(pfcp_header *header, uint16_t type, uint32_t value)
{
	sx_ie_recovery *ie = set_next_ie(header, type,sizeof(uint8_t));
	uint8_t *value_ptr = IE_TYPE_PTR_FROM_GTPV2C_IE(uint8_t, ie);

	*value_ptr = value;

	return get_ie_return(ie);
}

*/


uint16_t
set_recovery_ie(pfcp_header *header){
    sx_ie *ie = (sx_ie *) ( (uint8_t *) &header->seid_header + header->pfcp.mlength);
    ie->type = IE_RECOVERY_TIME_STAMP;
    ie->length =0 ;
    ie->value = (uint8_t *)time(NULL);
    
    header->pfcp.mlength = (header->pfcp.mlength) + sizeof(sx_ie);


    return 0;

}
